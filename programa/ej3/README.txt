Para iniciar el programa:
1) Ingrese en el CMD el comando make para instalar el programa
-> .../programa/ej3/make

2) Se creará un archivo SumadCuadrado, para ingresar a este haga lo siguiente:
-> ./infoUsuarios


En el programa:
1) El programa esta creado para guardar usuarios con las categorias de:
-> Nombre
-> Telefono
-> Saldo
-> Moroso

2) Como usuario, usted debe agregar cuantos usuarios desea agregar, llenando
   Las categorias anteriormente mencionadas

3) Al ingresar los datos, estos serán mostrados en el CMD

Para borrar el programa:
1) Asegurese que esté en la misma carpeta donde corrio el programa
-> .../programa/ej3

2) Ingrese el comando make clean para borrar el programa que uso
-> .../programa/ej3/make clean
