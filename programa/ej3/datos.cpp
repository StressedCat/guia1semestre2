#include <iostream>
using namespace std;
#include "datos.h"

/* constructores */
datos::datos(){
  string nombre;
  string numero;
  int saldo;
  bool moroso;
}

datos::datos(string nombre, string numero, int saldo, bool moroso){
  this->nombre = nombre;
  this->numero = numero;
  this->saldo = saldo;
  this->moroso = moroso;
}

/* get and set */
string datos::get_nombre(){
  return this->nombre;
}

string datos::get_numero(){
  return this->numero;
}

int datos::get_saldo(){
  return this->saldo;
}

bool datos::get_moroso(){
  return this->moroso;
}

void datos::set_nombre(string nombre){
  this->nombre = nombre;
}

void datos::set_numero(string numero){
  this->numero = numero;
}

void datos::set_saldo(int saldo){
  this->saldo = saldo;
}

void datos::set_moroso(bool moroso){
  this->moroso = moroso;
}

/* Esta funcion sera usada para mostrar la informacion de las personas */
void datos::print_all(int x){
  string name = this->nombre;
  string number = this->numero;
  int balan = this->saldo;
  bool delin = this->moroso;
  cout << "Persona numero " << x + 1 << endl;
  cout << "Nombre: " << name << endl;
  cout << "Telefono: " << number << endl;
  cout << "Saldo: " << balan << "$" << endl;
  if (delin == true) {
    cout << "Es moroso: Si" << endl;
  }
  else{
    cout << "Es moroso: No" << endl;
  }
  cout << "=========================================" << endl;
}
