#include <iostream>
using namespace std;

#ifndef DATOS_H
#define DATOS_H

class datos {
    private:
      string nombre;
      string numero;
      int saldo;
      bool moroso;

    public:
      /* constructores */
      datos();
      datos(string nombre, string numero, int saldo, bool moroso);

      /* set y get */
      string get_nombre();
      string get_numero();
      int get_saldo();
      bool get_moroso();
      void set_nombre(string nombre);
      void set_numero(string numero);
      void set_saldo(int saldo);
      void set_moroso(bool moroso);
      void print_all(int x);
};
#endif
