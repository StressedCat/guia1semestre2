#include <iostream>
#include <cstring>
#include <vector>
using namespace std;
#include "datos.h"

/* Para agregar informacion, se le pedira al usuario que ingrese los datos
 * del usuario, lo cual al final será agregado al array anteriormente creado
 */
void AGREGAR(string *Nombre, string *Telefono, int *Saldo, bool *Moroso, int x){
  datos n = datos();
  string name;
  string number;
  int balance;
  int YoN;
  bool delinquent;
  cout << "Ingrese el nombre:" << endl;
  cin >> name;
  cout << "Ingrese el numero:" << endl;
  cin >> number;
  cout << "Ingrese el saldo:" << endl;
  cin >> balance;
  cout << "Si es moroso, ingrese 0, caso contrario, cualquier tecla" << endl;
  cin >> YoN;
  if (YoN == 0) {
    delinquent = true;
  }
  else{
    delinquent = false;
  }

  n.set_nombre(name);
  n.set_numero(number);
  n.set_saldo(balance);
  n.set_moroso(delinquent);

  Nombre[x] = n.get_nombre();
  Telefono[x] = n.get_numero();
  Saldo[x] = n.get_saldo();
  Moroso[x] = n.get_moroso();
}

/* En el main se le pedira al usuario cuantos usuarios desea agregar
 * Siendo que la funcion para agregar datos se repitira x veces, segun el
 * usuario ingrese
 */
int main() {
  int tam;
  cout << "ingrese la cantidad de personas que serán agregadas" << endl;
  cin >> tam;
  string Nombre[tam];
  string Telefono[tam];
  int Saldo[tam];
  bool Moroso[tam];
  for (int x = 0; x < tam; x++){
    AGREGAR(Nombre, Telefono, Saldo, Moroso, x);
    cout << "=========================================" << endl;
  }

  /* Para imprimir la informacion, se usara una funcion que esta en datos.cpp
   * la cual consiste en mostrar todo los datos, categorizandolos con lo que
   * son, esto se realizara hasta que se muestren todas las personas ingresadas
   */
  datos Info = datos();
  for(int x = 0; x < tam; x++){
    Info.set_nombre(Nombre[x]);
    Info.set_numero(Telefono[x]);
    Info.set_saldo(Saldo[x]);
    Info.set_moroso(Moroso[x]);
    Info.print_all(x);
  }
  return 0;
}
