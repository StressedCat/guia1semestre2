#include <iostream>
#include <cctype>
#include <cstring>
using namespace std;

/*
 * Se mostrara el arreglo que se tiene, el cual ya estaba predeterminado
 * anteriormente
 */
void ShowArray(char *array, int tam){
  for (int x=0; x<tam; x++){
    cout << array[x];
  }
  cout << endl;
}

/*
 * Para ver cual es minuscula y cual es mayuscula se usaran las funciones
 * isupper() y islower(), junto con iterar todo el array de char, asi logrando
 * establecer un contador el cual nos dará el total de minusculas y mayusculas
 */
void ContadorMinyMayus(char *array, int tam) {
  int min = 0;
  int may = 0;
  for (int x = 0; x < tam ; x++){
    if (islower(array[x])) {
      min = min + 1;
    }
    else if (isupper(array[x])){
      may = may + 1;
    }
  }
  cout << "Las mayusculas totales en el texto son: " << may << endl;
  cout << "Las minusculas totales en el texto son: " << min << endl;
}

/*
 * El programa inicia con una frase ya determinada, la cual entra en
 * una funcion para ver cuantas minusculas tiene y cuantas mayusculas
 */
int main() {
  char Frase[] = "Mi Mama Me Mima, Pero aveces Pienso cual es el objetivo";
  int tam = *(&Frase + 1) - Frase;
  ShowArray(Frase, tam);
  ContadorMinyMayus(Frase, tam);
  return 0;
}
