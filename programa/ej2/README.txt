Para iniciar el programa:
1) Ingrese en el CMD el comando make para instalar el programa
-> .../programa/ej2/make

2) Se creará un archivo SumadCuadrado, para ingresar a este haga lo siguiente:
-> ./ContadorMinyMayus


En el programa:
1) El programa esta creado para tener una frase determinada, este siendo:
   "Mi Mama Me Mima"

2) El programa se encargará de ver cuantas mayusculas y minusculas se
   encuentran en el texto anterior y se visualizará en el CMD

Para borrar el programa:
1) Asegurese que esté en la misma carpeta donde corrio el programa
-> .../programa/ej2

2) Ingrese el comando make clean para borrar el programa que uso
-> .../programa/ej2/make clean
