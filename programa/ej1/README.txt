Para iniciar el programa:
1) Ingrese en el CMD el comando make para instalar el programa
-> .../programa/ej1/make

2) Se creará un archivo SumadCuadrado, para ingresar a este haga lo siguiente:
-> ./SumarCuadrados

En el programa:
1) Se le pedirá que ingrese el total de numeros que ingresará en el arreglo.

2) Al ingresar el el total de numeros que ingresará, se le pedira que ingrese
   el numero que desee.

3) Al ingresar todo los numeros, el programa realizará el proceso de mostrar
   cuales son sus numeros, como se ven elevados al cuadrado y cuanto es la
   suma de todos estos numeros.

Para borrar el programa:
1) Asegurese que esté en la misma carpeta donde corrio el programa
-> .../programa/ej1

2) Ingrese el comando make clean para borrar el programa que uso
-> .../programa/ej1/make clean
