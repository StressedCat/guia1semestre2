#include <iostream>
using namespace std;

void ShowArray(int *array, int tam){
  for (int x=0; x<tam; x++){
    cout << array[x] << endl;
  }
}

void Cuadrado(int *array, int tam, int *cuadrado) {
  int num;
  for (int x=0; x<tam; x++){
    num = array[x]*array[x];
    cuadrado[x] = num;
  }
}

void SumarArray(int *array, int tam){
  int suma = 0;
  for(int x=0; x<tam; x++){
    suma = array[x]+suma;
  }
  cout << "La suma del array al cuadrado es: " << suma << endl;
}

/*
 * Se le va a perdir al usuario que inicie el tamaño del arreglo, en donde
 * el usuario ingresaran numeros hasta que el tamaño sea alcanzado
 */
int main() {
  int tam;
  int num;
  cout << "Ingrese el tamaño de el arreglo: ";
  cin >> tam;
  int array[tam];
  int cuadrado[tam];
  cout << "Ingrese el numero que desea" << endl;
  for (int x=0; x<tam; x++){
    cout << "numero " << x+1 << endl;
    cin >> num;
    array[x] = num;
  }
  cout << endl;
  /*
   * Al realizar el paso anterior, el programa se encargara de mostrar
   * el arreglo que se inicio, al cuadrado y cual es la suma total de los que
   * estan al cuadrado
   */
  cout << "El array ingresado: " << endl;
  ShowArray(array, tam);
  Cuadrado(array, tam, cuadrado);
  cout << "El array al cuadrado: " << endl;
  ShowArray(cuadrado, tam);
  SumarArray(cuadrado, tam);
  return 0;
}
